package com.chaishop;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 * @EnableAdminServer ：声明服务是一个actuator admin服务端（admin ui）
 */

@SpringBootApplication
@EnableAutoConfiguration
@EnableAdminServer
public class ActuatorServerApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(ActuatorServerApp.class, args);
        System.out.println( "start ActuatorServerApp!" );
    }
}
