package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */

@SpringBootApplication
public class ActuatorClientApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(ActuatorClientApp.class, args);
        System.out.println( "start ActuatorClientApp!" );
    }
}
