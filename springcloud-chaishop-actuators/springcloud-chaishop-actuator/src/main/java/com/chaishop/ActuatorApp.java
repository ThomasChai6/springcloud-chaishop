package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: CPX
 * @Date: 2020/12/4 17:31
 * @version: 1.0
 */

@SpringBootApplication
public class ActuatorApp {

    public static void main(String[] args) {
        SpringApplication.run(ActuatorApp.class, args);
        System.out.println("start actuator");
    }
}
