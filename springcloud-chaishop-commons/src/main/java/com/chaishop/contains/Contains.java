package com.chaishop.contains;

/**
 * @Author: CPX
 * @Date: 2020/11/26 10:36
 * @version: 1.0
 */
public interface Contains {

     String HTTP_RES_CODE_200_VALUE = "success";
     String HTTP_RES_CODE_500_VALUE = "fail";

    //100类 ----用于指定客户端应相应的某些动作----
     Integer HTTP_CONTINUE = 100; //请求继续执行
     Integer HTTP_SWITCHING_PROTOCOLS = 101; //请求协议转换
     Integer HTTP_PROCESSING = 102; //

    //200类 ---用于表示请求成功---
     Integer HTTP_OK = 200; //请求OK
     Integer HTTP_CREATED = 202; //请求新创建文档
     Integer HTTP_NON_AUTHORITATIVE_INFOMATION = 203; //请求未授权 ---非官方信息---
     Integer HTTP_NO_CONTENT = 204; // 无内容
     Integer HTTP_RESET_CONTENT = 205; //内容重置 ---这个状态码用于强迫浏览器清除表单域--
     Integer HTTP_PARTIAL_CONTENT = 206; //局部内容 --服务器完成了一个包含Range头信息的局部请求时被发送的--
     Integer HTTP_MULTI_REPOFTED = 207;
     Integer HTTP_ALREADY_REPORTED = 208; //请求已经被转移
     Integer HTTP_IM_USED = 226;

    //300类 ---用于已经移动的文件并且常被包含在定位头信息中指定新的地址信息---
     Integer HTTP_MULTIPLE_CHOICES = 300; //请求被选择 ---多重选择---
     Integer HTTP_MOVED_RERANENTLY = 301; //状态是指所请求的文档在别的地方；文档新的URL会在定位响应头信息中给出。浏览器会自动连接到新的URL。

     Integer HTTP_FOUND = 302; //被找到
     Integer HTTP_SEE_OTHER = 303; //参见其他信息
     Integer HTTP_NOT_MODIFIED = 304; //未被修改 ---为修改---
     Integer HTTP_USE_PROXY = 305; //请求使用代理
     Integer HTTP_RESERVED = 306; //
     Integer HTTP_REMPORARY_REDIRECT = 307; //请求被临时转移了 ---临时重定向---
     Integer HTTP_PERMANENTLY_REDIRECT = 308; //

    //400类 用于指出客户端的错误
     Integer HTTP_BAD_REQUEST = 400; //错误请求 ---指出客户端请求中的语法错误---
     Integer HTTP_PARMENT_REQUIRED = 402; //
     Integer HTTP_FORBIDDEN = 403; //禁止访问 ---的意思是除非拥有授权否则服务器拒绝提供所请求的资源---
     Integer HTTP_NOT_FOUNF = 404; //无效的请求(为找到该服务)
     Integer HTTP_METHOD_NOT_ALLOWED = 405; //方法未允许 ---指出请求方法(GET, POST, HEAD, PUT, DELETE, 等)对某些特定的资源不允许使用---
     Integer HTTP_NOT_ACCEPTABLE = 406; //无法访问 ---示请求资源的MIME类型与客户端中Accept头信息中指定的类型不一致---
     Integer HTTP_PROXY_AUTHENTICATION_REQUEIRED = 407; //代理服务器认证要求
     Integer HTTP_REQUEST_TIMEOUT = 408; //请求超时
     Integer HTTP_CONFILICT = 409; //请求冲突
     Integer HTTP_GONE = 410; //请求的文件已经被移走
     Integer HTTP_LENGTH_REQUIRED = 411; // 需要数据长度 ---表示服务器不能处理请求（假设为带有附件的POST请求），除非客户端发送Content-Length头信息指出发送给服务器的数据的大小---
     Integer HTTP_PRECONDITION_FALLED = 412; //先决条件错误 ---状态指出请求头信息中的某些先决条件是错误的。---
     Integer HTTP_REQUEST_ENTITY_TOO_LARGE = 413; //请求实体过大
     Integer HTTP_REQUSET_URL_TOO_LONG = 414; //请求过长
     Integer HTTP_UNSUPPORTED_MEDIA_TYPE = 415; //请求的类型不支持
     Integer HTTP_REQUEST_RANGE_NOT_SATISFIABLE = 416;
     Integer HTTP_EXPECTATION_FAILED = 417; //期望失败
     Integer HTTP_I_AM_A_TEAPOT = 418;
     Integer HTTP_UNPROCESSABLE_ENTITY = 422;
     Integer HTTP_LOCKED = 423; //请求被锁定
     Integer HTTP_FAULED_DEPENDENCY = 424; //
     Integer HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;
     Integer HTTP_UPGRADE_REQUIRED = 426;
     Integer HTTP_PRECONDITION_REQUIRED = 428;
     Integer HTTP_TOO_MANY_REQYUSETS = 429; //请求太多
     Integer HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

    //500类
     Integer HTTP_INTERNAL_SERVER_ERROR = 500; //服务器内部出错
     Integer HTTP_NOT_IMPLEMENTED = 501; //未实现 ---状态告诉客户端服务器不支持请求中要求的功能---
     Integer HTTP_BAD_GATEWAY = 502; //错误的网关 ---被用于充当代理的服务器；该状态指出接收服务器接收远程服务器的错误响应
     Integer HTTP_SERVICE_UNAVAILABLE = 503; //服务无法获得 ---表示服务器在维护或者已经超载而无法响应
     Integer HTTP_GATEWAY_TIMEOUT = 504; //网关超时 ---代理或网关服务器，它指出的服务器没有从远端服务器得到及时的响应---
     Integer HTTP_VERSION_NOT_SUPPORTED = 505; //不支持HTTP版本
     Integer HTTP_VARIANT_ALSO_ENGOTIATED_EXPERIMENTAL = 506;
     Integer HTTP_INSUFFICIENT_STORAGE = 507;
     Integer HTTP_LOOP_DETECTED = 508;
     Integer HTTP_NOT_EXTENDED = 510;
     Integer HTTP_NETWORK_AUTHENTICATION_REQURED = 511;
}
