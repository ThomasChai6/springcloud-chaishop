package com.chaishop.response;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/11/26 10:23
 * @version: 1.0
 */

@Data
public class ResponseBase<T> {

    private String msg;
    private Integer code;
    private String data;

    public ResponseBase() {
    }

    public ResponseBase(String msg, Integer code, String data) {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }


}
