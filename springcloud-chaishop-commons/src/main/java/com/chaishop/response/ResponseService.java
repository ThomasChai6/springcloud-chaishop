package com.chaishop.response;

import com.chaishop.contains.Contains;

/**
 * @Author: CPX
 * @Date: 2020/11/26 10:30
 * @version: 1.0
 */
public class ResponseService {

    //设置错误响应结果
    public ResponseBase setResultError(String msg){
        return new ResponseBase(msg, Contains.HTTP_INTERNAL_SERVER_ERROR, "");
    }

    public ResponseBase setResultError(String msg,Integer code){
        return new ResponseBase(msg, code, "");
    }

    public ResponseBase setResultError(String msg,Integer code,String data){
        return new ResponseBase(msg, code, data);
    }

//    设置成功响应结果
    public ResponseBase setResultSuccess(String msg){
        return new ResponseBase(msg, Contains.HTTP_OK, "");
    }

    public ResponseBase setResultSuccess(String msg,Integer code ){
        return new ResponseBase(msg, code, "");
    }

    public ResponseBase setResultSuccess(String msg,Integer code,String data){
        return new ResponseBase(msg, code, data);
    }

    //设置通用响应结果
    public ResponseBase setResult(String msg,Integer code,String data){
        return new ResponseBase(msg, code, data);
    }

}
