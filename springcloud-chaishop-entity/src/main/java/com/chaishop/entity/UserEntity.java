package com.chaishop.entity;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/11/26 17:12
 * @version: 1.0
 */
@Data
public class UserEntity {

    private String name;
    private Integer age;

    public UserEntity() {
    }

    public UserEntity(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public UserEntity(String name) {
        this.name = name;
    }
}
