import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: CPX
 * @Date: 2020/12/16 16:07
 * @version: 1.0
 */
@SpringBootApplication
public class EntityApp {
    public static void main(String[] args) {
        SpringApplication.run(EntityApp.class, args);
    }
}
