package com.chaishop.zuulcontroller;

import com.chaishop.filter.TokenFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/12/2 15:07
 * @version: 1.0
 */

@RestController
public class ZullController {

    @Value("server.port")
    private String serverPort;

    @RequestMapping("/")
    public String zuulTest(){
        return "this is zuul server,端口为:"+serverPort;
    }
}
