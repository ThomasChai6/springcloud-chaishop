package com.chaishop.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author: CPX
 * @Date: 2020/12/2 16:30
 * @version: 1.0
 */
//@Component
@Slf4j
public class PostFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {

        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String method = request.getMethod();
        Cookie[] cookies = request.getCookies();
        String remoteUser = request.getRemoteUser();
        System.out.println("method:"+method);
        System.out.println("cookie[]:"+cookies);
        System.out.println("remoteUser:"+remoteUser);
       /* String jsonStr = "{\"method\"+:method,\"cokkie\":cookies[0]+\"\",\"remoteUser\":remoteUser}";
        String json = JSON.toJSONString(jsonStr);*/


        currentContext.setSendZuulResponse(true);
        currentContext.setResponseBody("method:"+method+",cookie:"+cookies[0]+",remoteUser:"+remoteUser);
        return true;
    }
}
