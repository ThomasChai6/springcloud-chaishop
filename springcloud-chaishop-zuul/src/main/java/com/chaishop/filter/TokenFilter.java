package com.chaishop.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: CPX
 * @Date: 2020/12/2 15:51
 * @version: 1.0
 * 必须要加@Component，将过滤器注入到spring容器中
 */
//@Component
public class TokenFilter extends ZuulFilter {
    /**
     * 请求类型，比如pre表示在请求之前进行处理
     * @return
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 多个请求的排序,值越大，优先级越高
     * @return
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 设置此过滤器是否生效
     * @return
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 接收到请求的处理逻辑
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext currentContext = RequestContext.getCurrentContext();
        //切记不能直接获取response然后对response直接操作，问题比较多
//        currentContext.getResponse();
        HttpServletRequest request = currentContext.getRequest();
        String userToken = request.getParameter("userToken");
        if(StringUtils.isBlank(userToken)){
            //不会继续执行，不会调用其他服务，直接将结果返回至客户端
            currentContext.setSendZuulResponse(false);
            currentContext.setResponseBody("userToken is null");
            currentContext.setResponseStatusCode(401);
            return false;
        }
        System.out.println("token 验证成功，可以访问");
        currentContext.setResponseBody("token验证成功，可以访问");
        return true;
    }
}
