package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @EnableConfigServer 分布式配置中心服务端注解，标识此服务为springCloud config server服务
 *  一、分布式配置管理中心原理
 *  1、分布式配置管理中心需要的组件
 *      1）web管理系统，通过可视化页面对配置文件进行管理（springCloud config server没有可视化页面进行管理）
 *      2）存放分布式配置文件服务器（持久化存储），一般为git和SVN
 *      3）缓存配置文件服务器：springCloud config server对配置文件进行缓存（存到内存中）
 *      4）springCloud config client读取SpringCloud config server端缓存数据
 *
 */

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class MemberConfServerApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(MemberConfServerApp.class, args);
        System.out.println( "start member configserver!" );
    }
}
