package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Member Config Client
 *
 */

@SpringBootApplication
@EnableEurekaClient
public class MemberConfigClientApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(MemberConfigClientApp.class, args);
        System.out.println( "start member config client!" );
    }
}
