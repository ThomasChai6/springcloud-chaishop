package com.chaishop.MemberController;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/12/1 15:45
 * @version: 1.0
 * @RefreshScope 使用actuator进行自动刷新读取配置中心配置
 */
@RestController
@RefreshScope
public class MemberController {
    @Value("${memberInfo}")
    private String memberInfo;

    @RequestMapping("/getMemberInfo")
    public String getMemberInfo(){
        return memberInfo;
    }
}
