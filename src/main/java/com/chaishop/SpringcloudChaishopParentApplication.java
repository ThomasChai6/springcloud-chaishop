package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudChaishopParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudChaishopParentApplication.class, args);
	}

}
