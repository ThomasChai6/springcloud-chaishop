package com.chaishop.services;

import com.chaishop.entity.UserEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author: CPX
 * @Date: 2020/11/26 17:14
 * @version: 1.0
 */
public interface MemberService {

    @RequestMapping("/getUser")
    public UserEntity getUser(@RequestParam("name") String name);

}
