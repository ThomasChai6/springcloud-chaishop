package com.chaishop.services;


import com.chaishop.entity.UserEntity;
import org.apache.catalina.User;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: CPX
 * @Date: 2020/11/26 17:33
 * @version: 1.0
 */
public interface OrderService {

    @RequestMapping("/getUser")
    public UserEntity getUser(String name);

    @RequestMapping("/getUserHystrix")
    public UserEntity getUserHystrix(String name);

}
