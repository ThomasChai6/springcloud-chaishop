package com.chaishop.feignservice;

import com.chaishop.entity.UserEntity;
import com.chaishop.services.MemberService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author: CPX
 * @Date: 2020/11/26 18:30
 * @version: 1.0
 */

/**
 * @FeignClient 的使用：能够方便的使用注解的方式进行调用服务
 * 1、@FeignClient注解要加到接口上
 * 2、@FeignClient实现的方法必须在实际调用类里面存在相应的方法，所以最好的办法是继承Member的接口类，同时也觉少了代码的重复
 */

@FeignClient("app-chaishop-member")
public interface FeignService extends MemberService{

}
