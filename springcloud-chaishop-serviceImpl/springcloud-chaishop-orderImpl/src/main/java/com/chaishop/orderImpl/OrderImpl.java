package com.chaishop.orderImpl;

import com.chaishop.entity.UserEntity;
import com.chaishop.feignservice.FeignService;
import com.chaishop.services.OrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/26 18:28
 * @version: 1.0
 */

/**
 * @RequestParam 必须加，不然会取不到name值
 */
@RestController
public class OrderImpl implements OrderService {

    @Autowired
    FeignService feignService;

    @RequestMapping("/getUser")
    public UserEntity getUser(@RequestParam("name") String name) {
        UserEntity user = null;
        try {
            //为了测试hystrix超时访问
            Thread.sleep(2000);
            user = feignService.getUser(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * 服务降级注解使用方式
     * @HystrixCommand: 使用FallBack回调函数，其还有如下作用
     * 1、HystrixCommand默认开启了隔离机制，以线程池方式（不同方法使用不用线程池，不会因为线程池线程满二阻塞）
     * 2、使用FallBack回到函数进行服务降级处理，给用户更好的体验效果
     * 3、默认开启了服务熔断机制，线程数为10
     */
    @HystrixCommand(fallbackMethod = "getUserHystrixFallBack")
    @RequestMapping("/getUserHystrix")
    public UserEntity getUserHystrix(String name) {
        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        return userEntity;
    }

    /**
     * 此方法可以优化成页面提示
     * 返回值必须与调用@HystrixCommand方法返回值一致
     * @return
     */
    public UserEntity getUserHystrixFallBack(String name){
        System.out.println("222");
        return new UserEntity();
    }

    @RequestMapping("/")
    public String zuulTest(){
        return "this is order server";
    }
}

