package com.chaishop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableEurekaClient
public class MemberApp2
{
    public static void main( String[] args )
    {
        SpringApplication.run(MemberApp2.class, args);
        System.out.println( "register member service" );
    }
}
