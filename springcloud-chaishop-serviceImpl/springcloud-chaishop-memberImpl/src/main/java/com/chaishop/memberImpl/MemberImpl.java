package com.chaishop.memberImpl;

import com.chaishop.entity.UserEntity;
import com.chaishop.services.MemberService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/26 17:29
 * @version: 1.0
 */

/**
 * 说明：
 * http://127.0.0.1:8020/getUser?name=chai会映射为http://app-chaishop-member/getUser?name=chai
 * 本质：通过feign去服务中心拉取服务信息进行服务转换
 * 真正访问的类上加上@RestController
 */
@RestController
public class MemberImpl implements MemberService {

    @Value("${server.port}")
    private String serverPort;

    @RequestMapping("/getUser")
    public UserEntity getUser(String name) {
        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        return userEntity;
    }

    @RequestMapping("/")
    public String zuulTest(){
        return "this is member server,端口号为:"+serverPort;
    }
}
